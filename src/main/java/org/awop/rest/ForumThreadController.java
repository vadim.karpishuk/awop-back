package org.awop.rest;

import org.awop.entity.request.ForumThreadRequest;
import org.awop.service.ForumThreadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/awop-back/forum-thread")
public class ForumThreadController {

    private ForumThreadService forumThreadService;

    @Autowired
    public ForumThreadController(ForumThreadService forumThreadService) {
        this.forumThreadService = forumThreadService;
    }

    @PostMapping("/create")
    public ResponseEntity createNewForumThread(@RequestBody ForumThreadRequest forumThreadRequest) {
        return forumThreadService.createNewForumThread(forumThreadRequest);
    }

}
