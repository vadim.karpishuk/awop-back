package org.awop.rest;

import org.awop.entity.User;
import org.awop.entity.UserDto;
import org.awop.entity.request.UserRequest;
import org.awop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api/v1/awop-back/user")
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/register")
    public ResponseEntity registerUser(@RequestBody UserRequest userRequest) {
        return userService.registerUser(userRequest);
    }

    @GetMapping("/preRegister/check/name/{username}")
    public ResponseEntity preRegisterUsername(@PathVariable(name = "username") String username) {
        if (userService.isExistsByUsername(username)) {
            return ResponseEntity.status(400).build();
        } else {
            return ResponseEntity.status(204).build();
        }
    }

    @GetMapping("/preRegister/check/email/{email}")
    public ResponseEntity preRegisterEmail(@PathVariable(name = "email") String email) {
        if (userService.isExistsByEmail(email)) {
            return ResponseEntity.status(400).build();
        } else {
            return ResponseEntity.status(204).build();
        }
    }

    @GetMapping(value = "/currentUser")
    public ResponseEntity getCurrentAuthUser(UsernamePasswordAuthenticationToken principal) {
        if (principal == null) {
            return ResponseEntity.status(404).build();
        }
        return ResponseEntity.status(200).body(userService.getCurrentAuthUser(principal.getName()));
    }

    @GetMapping("/getAll")
    public List<UserDto> getAllUsers() {
        return userService.getAllUsers();
    }

}
