package org.awop.rest;

import com.fasterxml.jackson.databind.JsonNode;
import org.awop.entity.Plugin;
import org.awop.service.PluginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/awop-back/plugins")
public class PluginController {

    private final PluginService pluginService;

    @Autowired
    public PluginController(PluginService pluginService) {
        this.pluginService = pluginService;
    }

    @GetMapping("/names")
    public List<String> getPluginNames() {
        return pluginService.getPluginNames();
    }

    @GetMapping("/{name}")
    public Plugin getPluginByName(@PathVariable("name") String name) {
        return pluginService.getPluginByName(name);
    }
}
