package org.awop.rest;

import com.fasterxml.jackson.databind.JsonNode;
import org.awop.entity.News;
import org.awop.entity.request.NewsRequest;
import org.awop.entity.response.NewsResponse;
import org.awop.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.Tuple;
import java.util.List;

@RestController
@RequestMapping("/api/v1/awop-back/news")
public class NewsController {

    private NewsService newsService;

    @Autowired
    public NewsController(NewsService newsService) {
        this.newsService = newsService;
    }

    @PostMapping("/create")
    public ResponseEntity createNews(@RequestBody NewsRequest newsRequest) {
        return newsService.createNews(newsRequest);
    }

    @GetMapping
    public JsonNode getNewsWithSort() {
        return newsService.getNewsWithCreateSort();
    }

}
