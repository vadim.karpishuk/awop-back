package org.awop.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Data
@Entity
@Table(name = "forum_thread")
public class ForumThread extends BaseObject {

    @JsonProperty(required = true)
    private String name;
    @JsonProperty(required = true)
    @Enumerated(EnumType.STRING)
    private Theme theme;
    @JsonProperty(required = true)
    @ManyToOne(targetEntity = User.class, cascade = CascadeType.ALL)
    private User creator;
    @OneToMany(mappedBy = "forumThread", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<Message> messages;
    private Timestamp createdAt;
    private Timestamp updatedAt;

    public enum Theme {
        QUESTION,
        BUG,
        SUGGESTION,
        ADMINISTRATION
    }

}
