package org.awop.entity;

import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.sql.Timestamp;

@Data
@Entity
@Table(name = "messages")
public class Message extends BaseObject {

    @ManyToOne(targetEntity = User.class, cascade = CascadeType.ALL)
    private User creator;
    private String text;
    @ManyToOne(targetEntity = ForumThread.class, cascade = CascadeType.ALL)
    private ForumThread forumThread;
    private Timestamp createdAt;
    private Timestamp updatedAt;

}
