package org.awop.entity.response;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonTypeIdResolver;
import lombok.Data;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@Data
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
@JsonSubTypes({
        @JsonSubTypes.Type(value = NewsResponse.class, name = "newsResponse"),
})
@JsonTypeIdResolver(CustomTypeIdResolver.class)
public class NewsResponse {

    private String topic;
    private String text;
    private UUID uuid;
    private Timestamp createdAt;
    private Timestamp updatedAt;
    private String creatorName;
    private UUID creatorUuid;
    private List<NewsResponse> internalNews;

}
