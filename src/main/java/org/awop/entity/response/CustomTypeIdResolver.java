package org.awop.entity.response;

import com.fasterxml.jackson.databind.DatabindContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.jsontype.impl.ClassNameIdResolver;
import com.fasterxml.jackson.databind.type.SimpleType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import java.io.IOException;

public class CustomTypeIdResolver extends ClassNameIdResolver {

    private static final String PREVIOUS_PATH = "org.awop.entity.response";
    private static final String PATH = "org.awop.entity.response.new";

    public CustomTypeIdResolver(JavaType baseType, TypeFactory typeFactory) {
        super(baseType, typeFactory);
    }

    public CustomTypeIdResolver() {
        super(SimpleType.constructUnsafe(Object.class), TypeFactory.defaultInstance());
    }

    @Override
    public JavaType typeFromId(DatabindContext context, String id) throws IOException {
        id = id.replace(PREVIOUS_PATH, PATH);
        return super.typeFromId(context, id);
    }

    @Override
    public String idFromValue(Object value) {
        String id = super.idFromValue(value);
        id = id.replace(PATH, PREVIOUS_PATH);
        return id;
    }
}
