package org.awop.entity;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@Entity
@Table(name = "users")
public class User extends BaseObject implements UserDetails {
    private boolean isActive;
    private Timestamp createdAt;
    private Timestamp lastActivity;
    @OneToMany(mappedBy = "creator")
    private List<ForumThread> forumThreadList;
    @OneToMany(mappedBy = "creator")
    private List<Message> messages;
    private String email;

    @ElementCollection(fetch = FetchType.EAGER)
    private Set<String> grantedAuthorities;
    private String password;
    private String name;
    private boolean accountNonExpired;
    private boolean accountNonLocked;
    private boolean credentialsNonExpired;
    private boolean enabled;

    public User(List<GrantedAuthority> grantedAuthorities, String password, String name, boolean accountNonExpired, boolean accountNonLocked, boolean credentialsNonExpired, boolean enabled) {
        this.grantedAuthorities = grantedAuthorities.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toSet());
        this.password = password;
        this.name = name;
        this.accountNonExpired = accountNonExpired;
        this.accountNonLocked = accountNonLocked;
        this.credentialsNonExpired = credentialsNonExpired;
        this.enabled = enabled;
    }

    public User() {

    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return grantedAuthorities.stream()
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toSet());
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return name;
    }

    @Override
    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public enum Role {
        ADMIN,
        MODERATOR,
        USER,
        SUBSCRIBED_USER
    }

}
