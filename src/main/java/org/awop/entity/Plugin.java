package org.awop.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Data
@Entity
@Table(name = "plugins")
public class Plugin extends BaseObject {

    private String name;
    @OneToMany(mappedBy = "plugin", fetch = FetchType.EAGER)
    private List<Release> releases;
    private String description;

}
