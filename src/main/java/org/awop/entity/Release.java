package org.awop.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.File;

@Data
@Entity
@Table(name = "releases")
public class Release extends BaseObject {

    @ManyToOne(targetEntity = Plugin.class)
    private Plugin plugin;
    private String version;
    private File file;

}
