package org.awop.entity.request;

import lombok.Data;
import org.awop.entity.User;

@Data
public class UserRequest {

    private String name;
    private User.Role role;
    private String password;
    private String email;

}
