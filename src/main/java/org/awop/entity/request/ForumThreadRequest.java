package org.awop.entity.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.awop.entity.ForumThread;

import java.util.UUID;

@Data
public class ForumThreadRequest {

    @JsonProperty(required = true)
    private String name;
    @JsonProperty(required = true)
    private ForumThread.Theme theme;
    @JsonProperty(required = true)
    private UUID creatorUuid;
    @JsonProperty(required = true)
    private MessageRequest messageRequest;

}
