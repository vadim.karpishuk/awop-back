package org.awop.entity.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.UUID;

@Data
public class NewsRequest {

    @JsonProperty(required = true)
    private UUID creatorUuid;
    @JsonProperty(required = true)
    private String topic;
    @JsonProperty(required = true)
    private String text;

}
