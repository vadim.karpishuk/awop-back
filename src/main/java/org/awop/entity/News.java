package org.awop.entity;

import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.sql.Timestamp;

@Data
@Entity
@Table(name = "news")
public class News extends BaseObject {

    @ManyToOne(targetEntity = User.class, cascade = CascadeType.ALL)
    private User creator;
    private String topic;
    private String text;
    private Timestamp createdAt;
    private Timestamp updatedAt;

}
