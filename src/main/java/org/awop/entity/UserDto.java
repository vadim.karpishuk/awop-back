package org.awop.entity;

import lombok.Data;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;


@Data
public class UserDto {

    private String name;
    private boolean isActive;
    private Timestamp createdAt;
    private Timestamp lastActivity;
    private List<UUID> forumThreadList;
    private List<UUID> messages;
    private UUID uuid;
    private String email;
}
