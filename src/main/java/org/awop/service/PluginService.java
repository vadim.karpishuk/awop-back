package org.awop.service;

import org.awop.entity.Plugin;
import org.awop.repository.PluginRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PluginService extends AbstractService<Plugin> {

    private PluginRepository pluginRepository;

    @Autowired
    public PluginService(PluginRepository pluginRepository) {
        super(pluginRepository);
        this.pluginRepository = pluginRepository;
    }

    public List<String> getPluginNames() {
        return pluginRepository.findAll().stream().map(Plugin::getName).collect(Collectors.toList());
    }

    public Plugin getPluginByName(String name) {
        return pluginRepository.findPluginByName(name);
    }
}
