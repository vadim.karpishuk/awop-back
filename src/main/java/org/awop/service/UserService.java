package org.awop.service;

import org.awop.entity.BaseObject;
import org.awop.entity.User;
import org.awop.entity.UserDto;
import org.awop.entity.request.UserRequest;
import org.awop.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserService extends AbstractService<User> implements UserDetailsService {

    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        super(userRepository);
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public ResponseEntity registerUser(UserRequest userRequest) {
        User user = new User(Collections.singletonList(new SimpleGrantedAuthority(User.Role.USER.toString())),
                passwordEncoder.encode(userRequest.getPassword()),
                userRequest.getName(),
                true,
                true,
                true,
                true); //TODO make false after
        Optional<User> optionalUser = userRepository.findUserByName(userRequest.getName());
        if(optionalUser.isPresent()) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(String.format("User with name %s is already present.", userRequest.getName()));
        }
        user.setActive(true);
        user.setCreatedAt(Timestamp.from(Instant.now()));
        user.setForumThreadList(new ArrayList<>());
        user.setMessages(new ArrayList<>());
        user.setName(userRequest.getName());
        user.setLastActivity(Timestamp.from(Instant.now()));
        user.setEmail(userRequest.getEmail());
        userRepository.save(user);
        return ResponseEntity
                .status(HttpStatus.CREATED).build();
    }

    public List<UserDto> getAllUsers() {
        List<UserDto> userDtos = new ArrayList<>();
        for (User user : userRepository.findAll()) {
            userDtos.add(convertToDto(user));
        }
        return userDtos;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository
                .findUserByName(username)
                .orElseThrow(() ->
                        new UsernameNotFoundException(String.format("Username %s not found", username)));
    }

    public UserDto getCurrentAuthUser(String username) {
        return convertToDto(userRepository.findUserByName(username).get());
    }

    public boolean isExistsByUsername(String username) {
        return userRepository.findUserByName(username).isPresent();
    }

    public boolean isExistsByEmail(String email) {
        return userRepository.findUserByEmail(email).isPresent();
    }

    private UserDto convertToDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setActive(user.isActive());
        userDto.setCreatedAt(user.getCreatedAt());
        userDto.setLastActivity(user.getLastActivity());
        userDto.setName(user.getName());
        userDto.setUuid(user.getUuid());
        userDto.setMessages(user.getMessages().stream().map(BaseObject::getUuid).collect(Collectors.toList()));
        userDto.setForumThreadList(user.getForumThreadList().stream().map(BaseObject::getUuid).collect(Collectors.toList()));
        userDto.setEmail(user.getEmail());
        return userDto;
    }
}
