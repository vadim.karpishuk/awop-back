package org.awop.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.awop.entity.News;
import org.awop.entity.User;
import org.awop.entity.request.NewsRequest;
import org.awop.entity.response.NewsResponse;
import org.awop.repository.NewsRepository;
import org.awop.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.persistence.Tuple;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.*;

@Service
public class NewsService extends AbstractService<News> {

    private NewsRepository newsRepository;
    private UserRepository userRepository;

    @Autowired
    public NewsService(NewsRepository newsRepository,
                       UserRepository userRepository) {
        super(newsRepository);
        this.newsRepository = newsRepository;
        this.userRepository = userRepository;
    }

    public ResponseEntity createNews(NewsRequest newsRequest) {
        Optional<User> optionalUser = userRepository.findById(newsRequest.getCreatorUuid());
        if(!optionalUser.isPresent()) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(String.format("There is no user with %s UUID", newsRequest.getCreatorUuid()));
        }
        News news = new News();
        news.setCreator(optionalUser.get());
        news.setCreatedAt(Timestamp.from(Instant.now()));
        news.setUpdatedAt(Timestamp.from(Instant.now()));
        news.setText(newsRequest.getText());
        news.setTopic(newsRequest.getTopic());
        newsRepository.save(news);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    public JsonNode getNewsWithCreateSort() {
        List<NewsResponse> newsResponses = new ArrayList<>();
        for(Object[] objects: newsRepository.getNewsWithCreateSort()) {
            NewsResponse newsResponse = new NewsResponse();
            newsResponse.setTopic((String) objects[0]);
            newsResponse.setText((String) objects[1]);
            newsResponse.setUuid((UUID) objects[2]);
            newsResponse.setCreatedAt((Timestamp) objects[3]);
            newsResponse.setUpdatedAt((Timestamp) objects[4]);
            newsResponse.setCreatorUuid((UUID) objects[5]);
            newsResponse.setCreatorName((String) objects[6]);
            newsResponses.add(newsResponse);
        }
//        List<NewsResponse> internal = new ArrayList<>();
//        internal.addAll(newsResponses);
//        newsResponses.get(0).setInternalNews(internal);
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.valueToTree(newsResponses);
    }
}
