package org.awop.service;

import org.awop.entity.Release;
import org.awop.repository.ReleaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReleaseService extends AbstractService<Release> {

    private ReleaseRepository releaseRepository;

    @Autowired
    public ReleaseService(ReleaseRepository releaseRepository) {
        super(releaseRepository);
        this.releaseRepository = releaseRepository;
    }
}
