package org.awop.service;

import org.awop.entity.ForumThread;
import org.awop.entity.Message;
import org.awop.entity.User;
import org.awop.entity.request.ForumThreadRequest;
import org.awop.repository.ForumThreadRepository;
import org.awop.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ForumThreadService extends AbstractService<ForumThread> {

    private ForumThreadRepository forumThreadRepository;
    private UserRepository userRepository;

    @Autowired
    public ForumThreadService(ForumThreadRepository forumThreadRepository, UserRepository userRepository) {
        super(forumThreadRepository);
        this.forumThreadRepository = forumThreadRepository;
        this.userRepository = userRepository;
    }

    public ResponseEntity createNewForumThread(ForumThreadRequest forumThreadRequest) {
        ForumThread forumThread = new ForumThread();
        Optional<User> userOptional = userRepository.findById(forumThreadRequest.getCreatorUuid());
        if(!userOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(String.format("Can't find user with %s uuid", forumThreadRequest.getCreatorUuid()));
        }
        forumThread.setCreator(userOptional.get());
        forumThread.setName(forumThreadRequest.getName());
        forumThread.setTheme(forumThreadRequest.getTheme());
        forumThread.setCreatedAt(Timestamp.from(Instant.now()));
        forumThread.setUpdatedAt(Timestamp.from(Instant.now()));
        List<Message> messages = new ArrayList<>();
        Message message = new Message();
        message.setCreator(userOptional.get());
        message.setText(forumThreadRequest.getMessageRequest().getText());
        message.setCreatedAt(Timestamp.from(Instant.now()));
        message.setUpdatedAt(Timestamp.from(Instant.now()));
        message.setForumThread(forumThread);
        messages.add(message);
        forumThread.setMessages(messages);
        forumThreadRepository.save(forumThread);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
