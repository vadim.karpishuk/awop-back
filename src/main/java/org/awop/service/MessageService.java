package org.awop.service;

import org.awop.entity.ForumThread;
import org.awop.entity.Message;
import org.awop.entity.User;
import org.awop.entity.request.MessageRequest;
import org.awop.repository.ForumThreadRepository;
import org.awop.repository.MessageRepository;
import org.awop.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Optional;

@Service
public class MessageService extends AbstractService<Message> {

    private MessageRepository messageRepository;
    private ForumThreadRepository forumThreadRepository;
    private UserRepository userRepository;

    @Autowired
    public MessageService(MessageRepository messageRepository,
                          ForumThreadRepository forumThreadRepository,
                          UserRepository userRepository) {
        super(messageRepository);
        this.messageRepository = messageRepository;
        this.forumThreadRepository = forumThreadRepository;
        this.userRepository = userRepository;
    }

    public ResponseEntity createNewMessageUnderThread(MessageRequest messageRequest) {
        if(messageRequest.getForumThreadUuid() == null) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body("You should provide UUID of forumThread");
        }
        Optional<ForumThread> optionalForumThread = forumThreadRepository.findById(messageRequest.getForumThreadUuid());
        if(!optionalForumThread.isPresent()) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(String.format("There is no forumThread with %s UUID", messageRequest.getForumThreadUuid()));
        }
        Optional<User> optionalUser = userRepository.findById(messageRequest.getCreatorUuid());
        if(!optionalUser.isPresent()) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(String.format("There is no user with %s UUID", messageRequest.getCreatorUuid()));
        }
        Message message = new Message();
        message.setCreator(optionalUser.get());
        message.setCreatedAt(Timestamp.from(Instant.now()));
        message.setText(messageRequest.getText());
        message.setForumThread(optionalForumThread.get());
        message.setUpdatedAt(Timestamp.from(Instant.now()));
        messageRepository.save(message);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
