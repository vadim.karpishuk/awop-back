package org.awop.service;

import org.awop.entity.BaseObject;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public abstract class AbstractService <O extends BaseObject> {

    private JpaRepository<O, UUID> repository;

    public AbstractService(JpaRepository<O, UUID> jpaRepository) {
        repository = jpaRepository;
    }

    public List<O> getAll() {
        return repository.findAll();
    }

    public O getByUuid(UUID uuid) {
        Optional<O> object = repository.findById(uuid);
        return object.orElse(null);
    }

    public void deleteByUuid(UUID uuid) {
        repository.deleteById(uuid);
    }

    public void deleteAll() {
        repository.deleteAll();
    }

    public void deleteAllInList(Iterable<O> iterable) {
        repository.deleteAll(iterable);
    }

    public O save (O entity) {
        return repository.save(entity);
    }

    public List<O> saveAll(List<O> list) {
        return repository.saveAll(list);
    }

    public Iterable<O> saveIterable(Iterable<O> iterable) {
        return repository.saveAll(iterable);
    }

    public boolean isExistsByUuid(UUID uuid) {
        return repository.existsById(uuid);
    }

}
