package org.awop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class AwopBack {

    public static void main(String args[]) {
        SpringApplication.run(AwopBack.class, args);
    }

}
