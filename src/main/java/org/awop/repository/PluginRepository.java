package org.awop.repository;

import org.awop.entity.Plugin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface PluginRepository extends JpaRepository<Plugin, UUID> {

    Plugin findPluginByName(String name);

}
