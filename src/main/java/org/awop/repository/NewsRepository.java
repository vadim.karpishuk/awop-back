package org.awop.repository;

import org.awop.entity.News;
import org.awop.entity.response.NewsResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.Tuple;
import java.util.List;
import java.util.UUID;

@Repository
public interface NewsRepository extends JpaRepository<News, UUID> {

    @Query("select news.topic, news.text, news.uuid, news.createdAt, news.updatedAt, news.creator.uuid, news.creator.name from News as news order by news.createdAt desc")
    List<Object[]> getNewsWithCreateSort();

}
